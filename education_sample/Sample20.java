public class Sample20 {

	public static void main(String[] args) {//メインメソッド
		
		KukuChecker checker = new KukuChecker();//インスタンス化、引数なしのKukuCheckerコンストラクタを呼び出し

		int[][] kuku = new int[9][9];//ローカル変数の定義、2次元配列、配列が２個（９と９）の引数のコンストラクタを呼び出し
		for (int i = 1; i <= 9; i++) {
			for (int j = 1; j <= 9; j++) {
				kuku[i - 1][j - 1] = i * j;
			}
		}
		System.out.println(checker.validate(kuku));//メソッドの呼び出し、KukuCheckerクラスを引数kukuで呼び出し
	}

}
