public class Sample19 {
	public static void main(String[] args) {//メインメソッド

		Calculator calc = new Calculator();//インスタンス化、引数なしのコンストラクタを呼びだし

		int year = 1987;
		int month = 11;
		System.out.println(calc.add(year, month));//メインメソッド、戻り値あり引数ありのメソッド呼び出し
	}
}
