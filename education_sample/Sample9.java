/*
演習
・この演習の目的・意図
	アクセス修飾子の基本的な書き方について確認するための演習です。
・課題
	下記のメインプログラムSample9において、本来であれば
	depositメソッドとwithdrawメソッドを通してのみ変更されるべき
	フィールドbalance（預金残高）を直接変更してしまっています。
	このままでは、口座の収支が合わなくなり、非常にまずい状態です。

	フィールドbalanceを直接修正できないようにAccount3クラスを修正してください。
	その結果、balanceを直接修正している行はコンパイルエラーになりますが、
	代わりにメソッドdeposit()を呼びだすコードに変更してエラーを解消してください。

	銀行口座ver3（Account3）クラス
	保有するフィールド：
			String name（口座名義）
			int no（口座番号）
			long balance（預金残高）
	保有するメソッド:
			void deposit(long amount)
				金額amountを口座に預けるメソッド。
				預けた金額は預金残高(balance)に反映(+)させること。
			void withdraw(long amount)
				金額amountを口座から引き出すメソッド。
				引き出した金額は預金残高(balance)に反映(-)させること。
			long getBalance()
				現在の預金残高を得るメソッド。

・期待する出力結果
	java Sample9
	-----最初の状態-----
	口座名義:銀行太郎 預金残高:0
	-----直接残高を修正-----
	口座名義:銀行太郎 預金残高:-1000
 */

public class Sample9 {

	public static void main(String[] args) {
		Account3 acc = new Account3();
		acc.name = "銀行太郎";
		acc.no = 100;

		System.out.println("-----最初の状態-----");//初期状態の定義を事前に行なってないと、0になる？
		System.out.println("口座名義:" + acc.name + " 預金残高:" + acc.getBalance());

		System.out.println("-----直接残高を修正-----");
		acc.deposit(-1000L);
		System.out.println("口座名義:" + acc.name + " 預金残高:" + acc.getBalance());

	}

}

class Account3 {
	String name; 	// 口座名義
	int no;			// 口座番号
	private long balance;	// 預金残高、privateを入れることによりアクセスルートを絞った

	/**
	 * 指定した金額を預金する。
	 * @param amount 預ける金額
	 */
	public void deposit(long amount){//balanceをベースにamountの入力をdepositを介してgetBalanceに反映、戻り値なし、引数あり
		balance += amount;
	}

	/**
	 * 指定した金額を口座から引き出す。
	 * @param amount 引き出す金額
	 */
	public void withdraw(long amount){//balanceをベースにamountの入力をwithdrawを介してgetBalanceに反映、戻り値なし、引数あり
		balance -= amount;
	}

	/**
	 * 現在の預金残高を返す。
	 * @return 現在の預金残高
	 */
	public long getBalance() {//getBalanceメソッドを定義してこのルートからアクセスできる、戻り値あり、引数なし、
		return balance;//戻り値があるため、returnが必要
	}

}
