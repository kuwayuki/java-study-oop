/*
演習
・この演習の目的・意図
	メソッドを作る際によくある間違いを認識するための演習です。
・課題
	Pastelクラスを扱うメインプログラムSample8を動作させた所、
	色番号に1を設定したはずが0と出力されてしまい、
	「期待する出力結果」と実際の出力結果が異なる。

	この原因を突きとめ修正してください。

・期待する出力結果
	java Sample8
	-----パステルの情報を出力-----
	このパステルのサイズは30、色番号は1
 */

public class Sample8 {

	public static void main(String[] args) {//メインメソッド
		Pastel pastelPen = new Pastel();//インスタンス化、引数なしのコンストラクタを呼び出す
		pastelPen.setSize(30);//引数30の戻り値なしを呼び出す
		pastelPen.setColor(1);//引数1の戻り値なしを呼び出す

		System.out.println("-----パステルの情報を出力-----");
		System.out.println("このパステルのサイズは" + pastelPen.size + "、色番号は" + pastelPen.color);

	}

}

class  Pastel {
	int size;	// パステルの太さ フィールド設定
	int color;	// パステルの色番号 フィールド設定

	/**
	 * パステルの太さの情報を得る。
	 * @return パステルの太さ
	 */
	public int getSize() {
		return size;
	}
	/**
	 * パステルの太さを設定する。
	 * @param size パステルの太さ
	 */
	public void setSize(int size) {
		this.size = size;
	}
	/**
	 * パステルの色番号を得る。
	 * @return 色番号
	 */
	public int getColor() {
		return color;
	}
	/**
	 * パステルの色番号を設定する。
	 * @color 色番号
	 */
	public void setColor(int color) {//color が colarになると値が0になる
		this.color = color;
	}



}
