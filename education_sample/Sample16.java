public class Sample16 {

	public static void main(String[] args) {//メインメソッド
		Day4 day1 = new Day4(2011,2,1);//インスタンス化、引数ありのコンストラクタを呼び出し
		Day4 day2 = new Day4(2011,4,1);//インスタンス化、引数ありのコンストラクタを呼び出し

		System.out.println(day1.toFormatString());//戻り値Stringと引数なしでメソッドを呼び出し
		System.out.println(day2.toFormatString());//戻り値Stringと引数なしでメソッドを呼び出し
	}

}

class Day4 {//設計図クラス
	private int year;	// 年、縛りを持ってフィールドを定義
	private int month;	// 月、縛りを持ってフィールドを定義
	private int day;	// 日、縛りをもってフィールドを定義
	private int dayOfWeek; //曜日、縛りをもってフィールドを定義

	private String[] dayNames = {"日","月","火","水","木","金","土"};//フィールドを配列で定義

	public Day4(int year, int month, int day) {//引数ありのコンストラクタを定義、条件を上書き、引数なしのコンストラクタは呼び出さないので定義しない
		this.year = year;//初期値を上書き
		this.month = month;//初期値を上書き
		this.day = day;//初期値を上書き

		int tempY = year;
		int tempM = month;
		// 1月、2月の場合は前年の13月、14月として扱う。
		if(month == 1 || month == 2){
			tempY--;
			tempM += 12;
		}
		// 曜日を求める公式（ツェラーの公式）
		this.dayOfWeek = (tempY + tempY/4 - tempY/100 + tempY/400 + (13 * tempM + 8) / 5 + day) % 7;//dayOfWeekの値を入力
	}

	public String toFormatString() {//メソッドを定義、戻り値あり、引数なし
		return year + "年" + month + "月" + day + "日" + "("+ dayNames[dayOfWeek] + ")";//戻り値があるのでreturn、dayNamesの配列[dayOfWeek]番目を呼出
	}
}
