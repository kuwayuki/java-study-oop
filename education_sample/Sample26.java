public class Sample26 {

	public static void main(String[] args) {

		boolean flag = true;

		Category1 c1 = new Category1();
		if (c1.getId() == null || c1.getName() == null) {
			flag = false;
		}
		if (!c1.getId().equals("NO_ID") || !c1.getName().equals("NO_NAME")) {
			flag = false;
		}
		c1.setId("a");
		c1.setName("b");
		if (!c1.getId().equals("a") || !c1.getName().equals("b")) {
			flag = false;
		}

		Category1 c2 = new Category1("c", "d");
		if (c2.getId() == null || c2.getName() == null) {
			flag = false;
		}
		if (!c2.getId().equals("c") || !c2.getName().equals("d")) {
			flag = false;
		}

		Category1 c3 = new Category1(null, null);
		if (!c3.getId().equals("NO_ID") || !c3.getName().equals("NO_NAME")) {
			flag = false;
		}

		Category1 c4 = new Category1("e", "f");
		c4.setId(null);
		c4.setName(null);
		if (!c4.getId().equals("NO_ID") || !c4.getName().equals("NO_NAME")) {
			flag = false;
		}

		String result = (flag) ? "OK" : "NG";
		System.out.println(result);
	}
}