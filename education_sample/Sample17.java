public class Sample17 {

	public static void main(String[] args) {//メインメソッド
		Day4 day1 = new Day4(2011,2,1);//インスタンス化、別のクラスから引数ありのコンストラクタを呼び出し
		Goods coffee1 = new Goods("101","キリマンジャロ",980L,day1);//インスタンス化、引数ありのコンストラクタを呼び出し

		Day4 day2 = new Day4(2011,4,29); //インスタンス化、別のクラスから引数ありのコンストラクタを呼び出し
		Goods coffee2 = new Goods("102","ブルーマウンテン",1450L,day2); //インスタンス化、引数ありのコンストラクタを呼び出し


		System.out.println(coffee1.toFormatString());//戻り値Stringを引数なしでメソッドを呼び出し
		System.out.println(coffee2.toFormatString());//戻り値Stringを引数なしでメソッドを呼び出し
	}

}

class Goods {//設計図クラス

	private String id;		// 商品のID、縛りを持ってフィールド定義
	private String name;	// 商品の名称、縛りを持ってフィールド定義
	private long price;		// 商品の価格、縛りを持ってフィールド定義
	private Day4 onSaleDay;	// 発売日、縛りを持ってフィールド定義、Day4クラスを呼び出し（型の中にクラスがある）

	public Goods(String id, String name, long price, Day4 onSaleDay) {//引数ありのコンストラクタを呼び出し、初期値を上書き
		this.id = id;
		this.name = name;
		this.price = price;
		this.onSaleDay = onSaleDay;
	}

	public String toFormatString() {//戻り値あり、引数なしでメソッドを定義
		// Goodsオブジェクトの内容を文字列として出力
		return  "商品ID:"+ id + " 商品名：" + name + " 価格:" + price + "円" + " 発売日:" + onSaleDay.toFormatString();//戻り値ある為returnあり、onSaleDay.toFormatString()を呼び出し（引数なし、戻り値あり）
	}

}