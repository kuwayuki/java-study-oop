/*
演習
・この演習の目的・意図
	コンストラクタの基本的な書き方について確認するための演習です。
・課題
	下記の仕様に従ってクラス(Day3)を作成してください。
	またそのDay3クラスを使って動作するメインプログラムSample13も作成してください。
	どのようなプログラムにするかは下記のメソッドに関する説明と期待する出力結果を参考にしてください。

	日付ver3（Day3）クラス
	保有するフィールド(全てprivateで作成)：
			int year(年)
			int month（月）
			int date（日）
	保有するコンストラクタ(全てpublicで作成)：
			Day3(int year,int month,int date)
				３つの引数はそれぞれ同名のフィールドを初期化する。
	保有するメソッド：
			String toFormatString()
				Dayオブジェクトが持つ年月日の各フィールドを整形して文字列として返す。
				文字列のフォーマットは"期待する出力結果"を参照
			※注：	ゲッターメソッドとセッターメソッドはこの演習では必要ありません。
					他の演習よりそれらをコピーしてきた場合は削除してください。

・期待する出力結果
	java Sample13
	1990年12月17日

 */

public class Sample13 {

	public static void main(String[] args) {
		Day3 anivDay = new Day3(1990,12,17);

		System.out.println(anivDay.toFormatString());
	}

}

class Day3 {
	private int year;	// 年
	private int month;	// 月
	private int day;	// 日

	public Day3(int year, int month, int day) {
		this.year = year;
		this.month = month;
		this.day = day;
	}

	public String toFormatString() {
		return year + "年" + month + "月" + day + "日";
	}
}
