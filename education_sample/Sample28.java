import java.util.List;

public class Sample28 {

	public static void main(String[] args) {

		boolean flag = true;

		Category c1 = new Category();
		if (c1.getId() == null || c1.getName() == null) {
			flag = false;
		}
		if (!c1.getId().equals("NO_ID") || !c1.getName().equals("NO_NAME")) {
			flag = false;
		}
		c1.setId("a");
		c1.setName("b");
		if (!c1.getId().equals("a") || !c1.getName().equals("b")) {
			flag = false;
		}

		Category c2 = new Category("c", "d");
		if (c2.getId() == null || c2.getName() == null) {
			flag = false;
		}
		if (!c2.getId().equals("c") || !c2.getName().equals("d")) {
			flag = false;
		}

		Category c3 = new Category(null, null);
		if (!c3.getId().equals("NO_ID") || !c3.getName().equals("NO_NAME")) {
			flag = false;
		}

		Category c4 = new Category("e", "f");
		c4.setId(null);
		c4.setName(null);
		if (!c4.getId().equals("NO_ID") || !c4.getName().equals("NO_NAME")) {
			flag = false;
		}
		
		Item i1 = new Item();
		if (i1.getId() == null || i1.getName() == null || i1.getPrice() < 0) {
			flag = false;
		}
		if (!i1.getId().equals("NO_ID") || !i1.getName().equals("NO_NAME") || i1.getPrice() != 0) {
			flag = false;
		}
		i1.setId("a");
		i1.setName("b");
		i1.setPrice(1);
		if (!i1.getId().equals("a") || !i1.getName().equals("b") || i1.getPrice() != 1) {
			flag = false;
		}

		Item i2 = new Item("c", "d", 2);
		if (i2.getId() == null || i2.getName() == null || i1.getPrice() < 0) {
			flag = false;
		}
		if (!i2.getId().equals("c") || !i2.getName().equals("d") || i2.getPrice() != 2) {
			flag = false;
		}

		Item i3 = new Item(null, null, -1);
		if (!i3.getId().equals("NO_ID") || !i3.getName().equals("NO_NAME") || i3.getPrice() != 0) {
			flag = false;
		}

		Item i4 = new Item("e", "f", 3);
		i4.setId(null);
		i4.setName(null);
		i4.setPrice(-1);
		if (!i4.getId().equals("NO_ID") || !i4.getName().equals("NO_NAME") || i4.getPrice() != 0) {
			flag = false;
		}

		Category category = new Category("g", "h");
		Item item1 = new Item("A", "B", 1);
		Item item2 = new Item("C", "D", 2);
		Item item3 = new Item(null, null, -1);
		category.setItem(item1);
		category.setItem(item2);
		category.setItem(item3);
		List<Item> items = category.getItems();
		if (items == null || items.size() != 3){
			flag = false;
		}
		if(!items.get(0).getId().equals("A") ||!items.get(0).getName().equals("B") ||items.get(0).getPrice() != 1){
			flag = false;
		}
		if(!items.get(1).getId().equals("C") ||!items.get(1).getName().equals("D") ||items.get(1).getPrice() != 2){
			flag = false;
		}
		if(!items.get(2).getId().equals("NO_ID") ||!items.get(2).getName().equals("NO_NAME") ||items.get(2).getPrice() != 0){
			flag = false;
		}
		
		String result = (flag) ? "OK" : "NG";
		System.out.println(result);
	}
}