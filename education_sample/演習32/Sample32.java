import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Sample32 {

	public static void main(String[] args) {

		// �@Mapオブジェクトの生成
		Map<Category, List<Item>> categoryMap = new LinkedHashMap<Category, List<Item>>();

		// �ACategoryオブジェクトの生成
		Category cateSB = new Category("SPORTS_BASEBALL", "野球用品");
		// �BListオブジェクトの生成
		List<Item> itemsSB = new ArrayList<Item>();
		// �CItemオブジェクトを生成し、Listオブジェクトに追加
		itemsSB.add(new Item("XB4862", "金属バット", 21000));
		itemsSB.add(new Item("R789T", "硬式グローブ", 18900));
		itemsSB.add(new Item("2H-3Z", "硬式ボール", 399));
		// �DMapにCategoryオブジェクトとItemのListオブジェクトに追加
		categoryMap.put(cateSB, itemsSB);

		// カテゴリ"SPORTS_KENDO"
		Category cateSK = new Category("SPORTS_KENDO", "剣道用品");
		List<Item> itemsSK = new ArrayList<Item>();
		itemsSK.add(new Item("TT-36", "胴張先細竹刀（３６）", 3000));
		itemsSK.add(new Item("MEN_XX", "4.5mm面ナナメ刺", 35000));
		categoryMap.put(cateSK, itemsSK);

		// カテゴリ"CULTURE_SHODO"
		Category cateCS = new Category("CULTURE_SHODO", "書道用品");
		List<Item> itemsCS = new ArrayList<Item>();
		itemsCS.add(new Item("RS27-2012", "写経セット「雅」", 6980));
		itemsCS.add(new Item("MP4-27", "端渓硯 麻子坑（長方）", 29800));
		categoryMap.put(cateCS, itemsCS);

		// �E以下、各カテゴリの要素を出力
		Set<Category> ｃategorySet = categoryMap.keySet();
		for (Category category : ｃategorySet) {
			System.out.println("■" + category.getName());
			for (Item item : categoryMap.get(category)) {
				System.out
						.println(item.getName() + ":" + item.getPrice() + "円");
			}
		}
	}
}
