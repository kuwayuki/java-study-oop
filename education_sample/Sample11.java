/*
演習
・この演習の目的・意図
	オーバーロードの基本的な記述方法について確認するための演習です。
・課題
	下記の仕様に従ってクラスを作成し、メインプログラムSample11が動作するようにしてください。

	数字関連のユーティリティ（NumberUtil）クラス
	保有するフィールド：
			なし
	保有するメソッド(全てpublicで作成)：
			メソッド名：
					max
			メソッドの機能：
					引数として与えられた数字の中で最大のものを返す。
					下記の引数と戻り値が異なる４つパターンのメソッドを４つ作成する。
					(1) int型の変数を二つ受け取り、その中の最大値をint型の戻り値として返す
					(2) int型の変数を三つ受け取り、その中の最大値をint型で戻り値として返す
					(3) double型の変数を二つ受け取り、その中の最大値をdouble型の戻り値として返す
					(4) double型の変数を三つ受け取り、その中の最大値をdouble型で戻り値として返す
					全て同じメソッド名(max)で作成すること。


・期待する出力結果
	java Sample11
	5と10で大きいのは10です。
	5と10と15で最大なのは15です。
	20.1と6.8で大きいのは20.1です。
	20.1と6.8と12.8で最大なのは20.1です。

 */
	public class Sample11 {

		public static void main(String[] args) {
			NumberUtil util = new NumberUtil();

			int a = 5;
			int b = 10;
			int c = 15;
			System.out.println(a + "と" + b + "で大きいのは" + util.max(a,b) + "です。");//41行目と同じメソッド名を使ってよし
			System.out.println(a + "と" + b + "と" + c + "で最大なのは" + util.max(a,b,c) + "です。");//4０行目と同じメソッド名を使ってよし

			double x = 20.1;
			double y = 6.8;
			double z = 12.8;

			System.out.println(x + "と" + y + "で大きいのは" + util.max(x,y) + "です。");
			System.out.println(x + "と" + y + "と" + z + "で最大なのは" + util.max(x,y,z) + "です。");
		}

	}

	class NumberUtil {//クラス、設計図

		public int max(int a, int b) {//メソッド定義、戻り値あり、引数あり
			int max = a;//初期値を入力
			if(b > max){//条件
				max = b;//書き換え
			}
			return max;//戻り値があるため
		}

		public int max(int a, int b, int c) {//メソッド定義、戻り値あり、引数あり
			int max = a;//初期値を入力
			if(b > max){//条件
				max = b;//書き換え
			}
			if(c > max){//条件
				max = c;//書き換え
			}
			return max;//戻り値があるため
		}

		public double max(double x, double y) {//メソッド定義、戻り値あり、引数あり
			double max = x;//初期値を入力
			if(y > max){//条件
				max = y;//書き換え
			}
			return max;//戻り値があるため
		}

		public double max(double x, double y, double z) {
			double max = x;
			if(y > max){
				max = y;
			}
			if(z > max){
				max = z;
			}
			return max;
		}

	}
