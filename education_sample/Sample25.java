public class Sample25 {

	public static void main(String[] args) {

		boolean flag = true;

		Category0 c1 = new Category0();
		if (c1.getId() != null || c1.getName() != null) {
			flag = false;
		}
		c1.setId("a");
		c1.setName("b");
		if (!c1.getId().equals("a") || !c1.getName().equals("b")) {
			flag = false;
		}

		Category0 c2 = new Category0("c", "d");
		if (c2.getId() == null || c2.getName() == null) {
			flag = false;
		}
		if (!c2.getId().equals("c") || !c2.getName().equals("d")) {
			flag = false;
		}

		String result = (flag) ? "OK" : "NG";
		System.out.println(result);
	}
}