/*
演習
・この演習の目的・意図
	mainメソッドのコマンドライン引数の取り扱い方を確認するための演習です。

・課題
	下記の「期待する出力結果」の通りの動作となるように、メインプログラムSample21を作成してください。

・期待する出力結果
	java Sample21 TURBO NA DOHC Hybrid
	受け取ったコマンドライン引数の数：4
	0個目のコマンドライン引数:TURBO
	1個目のコマンドライン引数:NA
	2個目のコマンドライン引数:DOHC
	3個目のコマンドライン引数:Hybrid
 */

public class Sample21 {

	public static void main(String[] args) {//メインメソッド

		System.out.println("受け取ったコマンドライン引数の数：" + args.length);//メソッドの呼び出し、argsの入力数合計

		for(int i=0 ; i < args.length ; i++ ){//繰り返し文法、初期値０、配列iは入力した数合計のマイナス１こ、１ずつ増える

			System.out.println( i + "個目のコマンドライン引数:" + args[i]);//めっそどの呼び出し、入力したargsを記載

		}
	}

}

