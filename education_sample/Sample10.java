/*
演習
・この演習の目的・意図
	カプセル化（データ隠蔽）の基本的な記述方法について確認するための演習です。
・課題
	下記の仕様に従ってクラスを作成し、メインプログラムSample10が動作するようにしてください。

	フィールドやメソッドは、カプセル化の考え方にのっとり適切なアクセス修飾子を付けるように
	してください。

	日付ver2（Day2）クラス
	保有するフィールド(全てprivateで作成)：
			int year(年)
			int month（月）
			int date（日）
	保有するメソッド(全てpublicで作成)：
			各フィールドのセッターメソッド、ゲッターメソッドを作成してください。
			セッターメソッド／ゲッターメソッドの命名規則は次のようにしてください。

				セッターメソッド → set + （頭文字を大文字にした）フィールド名
				ゲッターメソッド → get + （頭文字を大文字にした）フィールド名

				例：（フィールドyearの場合）
					セッターメソッド→setYear
					ゲッターメソッド→getYear

			その他の実装内容に関してはテキストのカプセル化の節を参考に作成してください。

・期待する出力結果
	java Sample10
	1990年12月17日
 */
	public class Sample10 {

		public static void main(String[] args) {//メインメソッド
			Day2 aDay = new Day2();//インスタンス化、引数なしのコンストラクタを呼び出し（初期値として、intの値が0で呼び出される）
			aDay.setYear(1990);//1990の引数で戻り値なしの呼び出して、上書き
			aDay.setMonth(12);//12の引数で戻り値なしの呼び出して、上書き
			aDay.setDay(17);//17の引数で戻り値なしの呼び出して、上書き

			System.out.print(aDay.getYear()    + "年");//printを使用して戻り値intのメソッド呼び出し（引数なし）
			System.out.print(aDay.getMonth()   + "月");//printを使用して戻り値intのメソッド呼び出し（引数なし）
			System.out.println(aDay.getDay()   + "日");//printを使用して戻り値intのメソッド呼び出し（引数なし）
		}

	}

	class Day2 {
		private int year;	// 年、フィールド設定
		private int month;	// 月、フィールド設定
		private int day;	// 日、フィールド設定

		public int getYear() {//メソッド定義、戻り値あり、引数なし
			return year;
		}
		public void setYear(int year) {//メソッド定義、戻り値なし、引数あり
			this.year = year;
		}
		public int getMonth() {//メソッド定義、戻り値あり、引数なし
			return month;
		}
		public void setMonth(int month) {//メソッド定義、戻り値なし、引数あり
			this.month = month;
		}
		public int getDay() {//メソッド定義、戻り値あり、引数なし
			return day;
		}
		public void setDay(int day) {//メソッド定義、戻り値なし、引数あり
			this.day = day;
		}

	}
