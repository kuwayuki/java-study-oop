/*
演習
・この演習の目的・意図
	キーボードからの入力を扱い方を学習するための演習です。

・課題
	キーボードから整数値を1つ入力してもらい、どんどん前の入力値に足し込んでいくプログラムSample30を作成してください。
	キーボードからの入力を受け付けるには、Javaの標準クラスライブラリの中に用意されているScannerクラスを利用します。
	基本的な使い方は下記を参照してください。
	なおキーボードからの入力は何回でも出来るものとし、0が入力された時だけ終了してください。
	終了するさいには「プログラムを終了しました。」と表示してください。

・期待する出力結果(数字だけの行はキーボードから入力した行)
	java Sample30
	整数を1つ入力してください。>
	10
	受け付けた値：10
	総合計：10
	整数を1つ入力してください。>
	20
	受け付けた値：20
	総合計：30
	整数を1つ入力してください。>
	100
	受け付けた値：100
	総合計：130
	整数を1つ入力してください。>
	0
	受け付けた値：0
	総合計：130
	プログラムを終了しました。
*/

// Scannerクラスを利用するために一番始めにこの記述が必要です。
// 詳しい説明は「パッケージ」について学習する章で扱います。
//import java.util.Scanner;
//
//public class Sample30 {
//
//	public static void main(String[] args) {
//
//		// まずはScannerクラスを生成しておきます。
//		// コンストラクタに与えているのは標準入力（つまりキーボード）オブジェクトです。
//		// このオブジェクトはJavaが自動的に用意しているオブジェクトです。
//		Scanner input = new Scanner(System.in);
//
//		System.out.println("整数を1つ入力してください。>");
//
//		// nextInt()は整数を1つキーボードから読み込むScannerクラスのメソッドです。
//		// その他に少数を読み込むnextDouble(), 文字列を読み込むnext()等が用意されています。
//		int inputedNumber = input.nextInt();
//
//		System.out.println("受け付けた値：" + inputedNumber);
//
//	}
//
//}

// 解答例
import java.util.Scanner;

public class Sample24 {

	public static void main(String[] args) {

		int inputedNumber = 0;	// 入力された値
		int total = 0;			// これまでに入力された値の総合計

		Scanner input = new Scanner(System.in);

		do{
			System.out.println("整数を1つ入力してください。>");
			inputedNumber = input.nextInt();
			System.out.println("受け付けた値：" + inputedNumber);

			total += inputedNumber;	// 合計の計算
			System.out.println("総合計：" + total);
		} while(inputedNumber != 0);

		System.out.println("プログラムを終了しました。");

	}

}

