public class Sample29 {

	public static void main(String[] args) {
		Category category = new Category("SPORTS_BASEBALL", "野球用品");

		Item[] items = new Item[3];
		items[0] = new Item("XB4862", "金属バット", 21000);
		items[1] = new Item("R789T", "硬式グローブ", 18900);
		items[2] = new Item("2H-3Z", "硬式ボール", 399);

		for (Item item : items) {
			category.setItem(item);
		}

		for (Item item : category.getItems()) {
			System.out.println(item.getName());
		}
	}

}
