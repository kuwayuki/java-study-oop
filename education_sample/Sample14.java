/*
演習
・この演習の目的・意図
	コンストラクタを作る際によくある間違いを認識するための演習です。
・課題
	Pastel2クラスを扱うメインプログラムSample14を動作させた所、
	コンストラクタを使って初期値を設定したにも関わらず、出力結果は全て0と
	出力されてしまい、「期待する出力結果」と実際の出力結果が異なる。

	この原因を突きとめ修正してください。

・期待する出力結果
	java Sample43
	このパステルのサイズは30, 色番号は1
 */

public class Sample14 {

	public static void main(String[] args) {

		Pastel2 crayon = new Pastel2();
		System.out.println(crayon.toString());

	}
}

class  Pastel2 {
	private int size;	// パステルの太さ
	private int color;	// パステルの色番号

	/**
	 * Pastel2クラスのコンストラクタ
	 * デフォルトの値(size=30,color=1)をセットする。
	 */
	public Pastel2(){
		// デフォルトの値を設定
		this.size = 30;
		this.color = 1;
	}

	public String toString() {
		return "このパステルのサイズは" + size + ", 色番号は" + color;
	}


}
