/*
演習
・この演習の目的・意図
	オーバーロードで陥りがちな事象について確認するための演習です。
・課題
	下記の仕様に従ってクラス(PaintBrush)を作成してください。
	またそのPaintBrushクラスを使って動作するメインプログラムSample12も作成してください。
	どのようなプログラムにするかは下記のメソッドに関する説明と期待する出力結果を参考にしてください。

	PaintBrush（筆）クラス
	保有するフィールド：
			なし
	保有するメソッド(全てpublicで作成)：
			メソッド名：
					分かりやすい名前を自由に付けてください。
			メソッドの機能：
					引数として与えられた座標で図形を描く。
					（ただし実際に描画するのではなく「四角形を座標(x1,y1,x2,y2)で描画しました」とメッセージを出すだけ）
					（x1,y1,x2,y2の部分は実際には引数として与えられた値を表示する。）
					（例えばx1,y1,x2,y2はそれぞれ(5,7,15,20)などの値がはいるものとする。）

					引数と表示メッセージのパターンは以下のとおり。
					・int型の変数(x1,y1)２つとdouble型の変数(x2,y2)２つを受け取り「四角形を座標(x1,y1,x2,y2)で描画しました」と表示する。
					・double型の変数(x1,y1)２つとint型の変数(x2,y2)２つを受け取り「直線を座標(x1,y1,x2,y2)で描画しました」と表示する。
					・int型の変数(x,y,z)を３つ受け取り「円を座標(x,y)を原点に半径zで描画しました」と表示する。
					以上、３つのメソッドを作成してください。



・期待する出力結果

	四角形を座標(5,7,15.0,20.0)で描画しました
	直線を座標(5.0,7.0,15,20)で描画しました
	円を座標(10,20)を原点に半径5で描画しました

 */
public class Sample12 {

	public static void main(String[] args) {
		PaintBrush brush = new PaintBrush();

		brush.draw(5,7,15.0,20.0);
		brush.draw(5.0,7.0,15,20);
		brush.drawCircle(10,20,5);
	}
}

class PaintBrush {

	public void draw(int x1, int y1, double x2, double y2) {

		System.out.println("四角形を座標(" + x1 + "," + y1 + "," + x2 + "," + y2 + ")で描画しました");

	}

	public void draw(double x1, double y1, int x2, int y2) {

		System.out.println("直線を座標(" + x1 + "," + y1 + "," + x2 + "," + y2 + ")で描画しました");

	}


	public void drawCircle(int x, int y, int z) {

		System.out.println("円を座標(" + x + "," + y + ")を原点に半径" + z + "で描画しました");

	}

}
