/*
演習
・この演習の目的・意図
	メソッドを持っているクラスの基本的な書き方について確認するための演習です。
・課題
	下記の仕様に従ってクラスを作成し、メインプログラムSample2が動作するようにしてください。
	日付（Day）クラス
	保有するフィールド：
			int year(年)
			int month（月）
			int date（日）
	保有するメソッド：
			String toFormatString()
				Dayオブジェクトが持つ年月日の各フィールドを整形して文字列として返す。
				文字列のフォーマットは"期待する出力結果"を参照

・期待する出力結果
	java Sample2
	1990年12月17日

 */

public class Sample2 { //クラス

	public static void main(String[] args) {//メインメソッド
		Day birthDay = new Day();//インスタンス化 引用なしのコンストラクタが呼び出される
		birthDay.year = 1990;//数値を入力
		birthDay.month = 12;//数値を入力
		birthDay.day = 17;//数値を入力

		System.out.println(birthDay.toFormatString());//標準メソッドで引数なしのメソッドを呼び出すと戻り値が表示
	}

}

class Day {//クラス設計図
	int year;	// 年//フィールド
	int month;	// 月//フィールド
	int day;	// 日//フィールド

	String toFormatString() {
		return year + "年" + month + "月" + day + "日";//引数なしで戻り値があるメソッドを定義、returnがいる
	}
}
