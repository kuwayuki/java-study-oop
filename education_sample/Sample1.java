/*
演習
・この演習の目的・意図
	クラスの基本的な書き方について確認するための演習です。
・課題
	下記の仕様に従ってクラスを作成し、メインプログラムSample1が動作するようにしてください。
	銀行口座（Account）クラス
	保有するフィールド：
			String name（口座名義）
			int no（口座番号）
			long balance（預金残高）
	保有するメソッド:
			なし
・期待する出力結果
	java Sample1
	口座名義:銀行太郎 口座番号:100 預金残高:10000

 */

public class Sample1 {//クラス publicをつけることによって違うパッケージだとしてもAccountを使えるようにしてる

	public static void main(String[] args) {//メインメソッド
		Account acc = new Account();//インスタンス化と引用なしのコンストラクタが呼ばれる
		acc.name = "銀行太郎"; //値を入力
		acc.no = 100; //値を入力
		acc.balance = 10000L; //値を入力

		System.out.println("口座名義:" + acc.name + " 口座番号:" + acc.no + " 預金残高:" + acc.balance);//標準メソッドで呼び出し
	}

}

class Account {//クラス 設計図 パッケージ内、継承内
	String name; 	// 口座名義 //フィールド
	int no;			// 口座番号 //フィールド
	long balance;	// 預金残高 //フィールド
}
