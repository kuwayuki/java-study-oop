public class Sample27 {

	public static void main(String[] args) {

		boolean flag = true;

		Item i1 = new Item();
		if (i1.getId() == null || i1.getName() == null || i1.getPrice() < 0) {
			flag = false;
		}
		if (!i1.getId().equals("NO_ID") || !i1.getName().equals("NO_NAME") || i1.getPrice() != 0) {
			flag = false;
		}
		i1.setId("a");
		i1.setName("b");
		i1.setPrice(1);
		if (!i1.getId().equals("a") || !i1.getName().equals("b") || i1.getPrice() != 1) {
			flag = false;
		}

		Item i2 = new Item("c", "d", 2);
		if (i2.getId() == null || i2.getName() == null || i1.getPrice() < 0) {
			flag = false;
		}
		if (!i2.getId().equals("c") || !i2.getName().equals("d") || i2.getPrice() != 2) {
			flag = false;
		}

		Item i3 = new Item(null, null, -1);
		if (!i3.getId().equals("NO_ID") || !i3.getName().equals("NO_NAME") || i3.getPrice() != 0) {
			flag = false;
		}

		Item i4 = new Item("e", "f", 3);
		i4.setId(null);
		i4.setName(null);
		i4.setPrice(-1);
		if (!i4.getId().equals("NO_ID") || !i4.getName().equals("NO_NAME") || i4.getPrice() != 0) {
			flag = false;
		}

		String result = (flag) ? "OK" : "NG";
		System.out.println(result);
	}
}