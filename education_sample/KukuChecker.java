public class KukuChecker
{

    public boolean validate(int ai[][])//戻り値ありのメソッドを定義、引数あり（配列２個）、九九の配列が成り立っているか確認するメソッド
    {
        if(ai == null || ai.length != 9 || ai[0].length != 9)//nullである、行が９個ない、０行目の列が９個ない
            return false;//誤り
        for(int i = 1; i <= 9; i++)
        {
            for(int j = 1; j <= 9; j++)
                if(ai[i - 1][j - 1] != i * j)
                    return false;
        }
        return true;
    }
}
