public class Sample18 {

	public static void main(String[] args) {//メインメソッド
		Day4 day1 = new Day4(2011, 1, 20);//インスタンス化、引数ありのコンストラクタを呼び出し、違うクラスのDay4を呼び出している
		Goods toy1 = new Goods("101", "アクションフィギュア№100", 380L, day1);//インスタンス化、引数ありのコンストラクタを呼び出し、違うクラスのGoodsを呼び出している

		Day4 day2 = new Day4(2011, 3, 14);//インスタンス化、引数ありのコンストラクタを呼び出し、違うクラスのDay4を呼び出している
		Goods toy2 = new Goods("102", "35周年記念プラモデル", 280L, day2);//インスタンス化、引数ありのコンストラクタを呼び出し、違うクラスのGoodsを呼び出している

		Day4 orderDay = new Day4(2011, 5, 7);//インスタンス化、引数ありのコンストラクタを呼び出し、違うクラスのDay4を呼び出している
		Order order1 = new Order("1001",toy1,5L,orderDay);//インスタンス化、引数ありのコンストラクタを呼び出し
		Order order2 = new Order("1002",toy2,15L,orderDay);//インスタンス化、引数ありのコンストラクタを呼び出し

		System.out.println(order1.toFormatString());//メソッドの呼び出し
		System.out.println(order2.toFormatString());//メソッドの呼び出し
	}

}

class Order {//設計クラス

	private String id;		// 注文ID、フィールドの定義
	private Goods product;	// 注文された商品、フィールドの定義
	private long amount;	// 注文された数量、フィールドの定義
	private Day4 orderDay;	// 受注日、フィールドの定義、Day4クラスを呼び出し（型の中にクラスがある）

	public Order(String id, Goods product, long amount, Day4 orderDay) {//コンストラクタを定義、引数あり、初期値を上書き
		this.id = id;//左のidは22行目のこと、右のidは27行目のこと
		this.product = product;
		this.amount = amount;
		this.orderDay = orderDay;
	}

	public String toFormatString() {//メソッドを定義（戻り値あり、引数なし）
		// Orderオブジェクトの内容を文字列として出力
		return "注文ID:" + id + " 注文商品：" + product.toFormatString() + " 数量:"//戻り値ある為returnあり、Goods・Day4クラスのメソッド呼び出し
				+ amount + " 受注日:" + orderDay.toFormatString();
	}

}