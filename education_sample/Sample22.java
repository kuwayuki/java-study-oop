/*
演習
・この演習の目的・意図
	staticの基本的な書き方について確認するための演習です。
・課題
	下記の仕様に従ってクラスを作成し、メインプログラムSample24が動作するようにしてください。
	Decoratorクラス
	保有するフィールド(全てpublicかつstaticで作成)：
			int number（デコレートする'*'の個数）

	保有するメソッド(全てpublicかつstaticで作成)：
			String decorate(String target)
				引数targetの前後をnumber個分の'*'でデコレートし戻り値として返すメソッド。
				<例>
				引数で与えた文字列				:	BANANA
				フィールドnumberの数			:	4
				このメソッドから返される文字列	:	****BANANA****

・期待する出力結果
	java Sample24
	****BANANA****

 */

public class Sample22 {

	public static void main(String[] args) {//メインメソッドを定義

		Decorator.number = 4;//メソッドに数値を入力
		System.out.println(Decorator.decorate("BANANA"));//戻り値ありのメソッドを呼び出し、引数あり

	}

}

class Decorator {//設計図クラス

	/**
	 * デコレートする*の数
	 */
	public static int number;//フィールドを定義、staticをつけてクラス全体で使うことができるので、numberを使うときにインスタンス化しなくてよい

	/**
	 * 引数targetの前後をnumber個分の'*'でデコレートし戻り値として返すメソッド。
	 * @param target デコレートしたい文字列
	 * @return デコレートした結果
	 */
	public static String decorate(String target){//メソッドを定義、戻り値あり、引数あり

		String result = "";//ローカル変数を定義
		String asterisks = "";//ローカル変数を定義

		for(int i = 0; i < number ; i++){
			asterisks += "*";
		}
		result = asterisks + target + asterisks;//メソッドの中身
		return result;//戻り値ありだから、returnあり

	}
}
