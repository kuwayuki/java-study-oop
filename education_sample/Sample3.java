/*
演習
・この演習の目的・意図
	メソッドを持っているクラスの基本的な書き方について確認するための演習です。
・課題
	下記の仕様に従ってクラスを作成し、メインプログラムSample3が動作するようにしてください。
	銀行口座ver2（Account2）クラス
	保有するフィールド：
			String name（口座名義）
			int no（口座番号）
			long balance（預金残高）
	保有するメソッド:
			void deposit(long amount)
				金額amountを口座に預けるメソッド。
				預けた金額は預金残高(balance)に反映(+)させること。
			void withdraw(long amount)
				金額amountを口座から引き出すメソッド。
				引き出した金額は預金残高(balance)に反映(-)させること。

・期待する出力結果
	java Sample3
	-----最初の状態-----
	口座名義:銀行太郎 預金残高:10000
	-----1000を預金-----
	口座名義:銀行太郎 預金残高:11000
	-----10000を引き出し-----
	口座名義:銀行太郎 預金残高:1000
 */

public class Sample3 {//クラス

	public static void main(String[] args) {//メインメソッド
		Account2 acc = new Account2();//インスタンス化、引数なしのコンストラクタを呼び出す
		acc.name = "銀行太郎";//値を入力
		acc.no = 100;//値を入力
		acc.balance = 10000L;//値を入力

		System.out.println("-----最初の状態-----");//標準メソッドで呼び出し
		System.out.println("口座名義:" + acc.name + " 預金残高:" + acc.balance);//標準メソッドで呼び出し

		System.out.println("-----1000を預金-----");//標準メソッドで呼び出し
		acc.deposit(1000);//メソッドを呼び出し、引数ありのため、10000+1000が呼び出される
		System.out.println("口座名義:" + acc.name + " 預金残高:" + acc.balance);//標準メソッドで呼び出し

		System.out.println("-----10000を引き出し-----");//標準メソッドで呼び出し
		acc.withdraw(10000);//メソッドを呼び出し、引数ありのため、10000-10000が呼び出される
		System.out.println("口座名義:" + acc.name + " 預金残高:" + acc.balance);//メ標準メソッドで呼び出し

	}

}

class Account2 {//クラス、設計図
	String name; 	// 口座名義、フィールド
	int no;			// 口座番号、フィールド
	long balance;	// 預金残高、フィールド

	/**
	 * 指定した金額を預金する。
	 * @param amount 預ける金額
	 */
	void deposit(long amount){//引数あり、戻り値なし
		balance += amount;//amountが足されるメ標準メソッドで呼び出し
	}

	/**
	 * 指定した金額を口座から引き出す。
	 * @param amount 引き出す金額
	 */
	void withdraw(long amount){//メソッド、引数あり、戻り値なし
		balance -= amount;//amountが引かれるメソッド
	}

}
