/*
演習
・この演習の目的・意図
	コンストラクタにまつわるよくある間違いを認識するための演習です。
・課題
	Pastel3クラスに下記のように２つの引数を受け取るコンストラクタを
	追加して、メインプログラムSample15が動作するようにしてください。
	もし必要であればPastel3にその他の修正をしてもかまいません。

	保有するコンストラクタ(全てpublicで作成)：
			Pastel3(int size,int color)
				２つの引数はそれぞれ同名のフィールドを初期化する。

・期待する出力結果
	java Sample44
	このパステルのサイズは50, 色番号は2
	このパステルのサイズは60, 色番号は3

 */

public class Sample15 {//クラス

	public static void main(String[] args) {//メインメソッド

		Pastel3 crepath = new Pastel3();//インスタンス化、引数なしのコンストラクタを呼び出し
		crepath.setSize(50);//引数50でメソッドを呼び出し
		crepath.setColor(2);//引数2でメソッドを呼び出し
		System.out.println(crepath.toString());//戻り値Stringでメソッドを呼び出し

		Pastel3 pastel = new Pastel3(60,3); // コンストラクタを追加する事でこのエラーを解消して、インスタンス化と引数ありのコンストラクタを呼び出し
		System.out.println(pastel.toString());//戻り値Stringでメソッドを呼び出し

	}
}

class  Pastel3 {//クラス、設計図
	private int size;	// パステルの太さ、フィールド
	private int color;	// パステルの色番号、フィールド

	/**
	 * Pastel3の引数なしのコンストラクタ。
	 * 引数を取るコンストラクタを１つでも作ってしまった場合は
	 * このコンストラクタも必要となる。
	 */
	public Pastel3(){//引数なしのコンストラクタを定義、引数ありのコンストラクタが別途存在することにより作成

	}

	/**
	 * Pastel3のコンストラクタ。２つの引数を取る
	 * @param size パステルの太さの初期//引数を取れる話
	 * @param colorパステルの色番号の初期値
	 */
	public Pastel3(int size, int color) {//引数ありのコンストラクタを定義
		this.size = size;
		this.color = color;
	}

	public void setSize(int size) {//メソッドを定義、戻り値なし、引数あり
		this.size = size;
	}

	public void setColor(int color) {//メソッドを定義、戻り値なし、引数あり
		this.color = color;
	}

	public String toString() {//メソッドを定義、戻り値あり、引数なし
		return "このパステルのサイズは" + size + ", 色番号は" + color;
		// String a = "このパステルのサイズは" + size + ", 色番号は" + color;
	  // return a ;
	}

}
